'''
Created on Aug 7, 2015

@author: Minh Ngoc Le
'''

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import spearmanr


if __name__ == '__main__':
    m = 100
    n = 100
    gs = np.sort(np.random.rand(m))
    tar = np.repeat(gs.reshape(1, -1), n, 0)
    tar[n/2:] = 1-tar[n/2:]
    rhos = []
    for k in range(n):
        switches = np.random.randint(m, size=(np.random.randint(m),2))
        p, q = switches[:,0], switches[:,1]
        tar[k,p], tar[k,q] = tar[k,q], tar[k,p]
        rhos.append(spearmanr(gs, tar[k])[0])
    rhos = np.array(rhos)
    for i, focal in enumerate([0.42, 0.45, 0.47, 0.52, 0.55]):
        index = np.argmin(np.absolute(rhos-focal))
        plt.subplot(3, 3, i+1)
        plt.title("rho=%.1f" %rhos[index])
        plt.axis("off")
        plt.plot(gs, 'g', tar[index], 'r')
    plt.show()