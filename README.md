Code for paper "M. N. Le, A. Fokkens. Taxonomy beats corpus in similarity identification, but does it matter? RANLP 2015."

## Reproducibility notes

### 1. Obtain data and update paths

Download `GoogleNews-vectors-negative300.bin.gz` from 
[word2vec project page](https://code.google.com/p/word2vec/#Pre-trained_word_and_phrase_vectors)
and extract it to your local machine.

Update path to the file in `exp_word2vec.py`.

Download `deps.words.bz2` from
from [Levy's blog](https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/)
and extract it to your local machine.

Update path to the file in `exp_deps.py`.

### 2. Run experiments

These are all the needed programs to reproduce results results in the paper:

- `exp_wordnet.py`
- `exp_word2vec.py` 
- `exp_deps.py`

The listing is lengthy but once you read it closely, it should be self-describing. 

### 3. Plot ordering accuracy vs. Spearman's rho

Run `simulate_ordering_accuracy.py`, the plot should appear in a new window
(you need `matplotlib`).

## FAQ

Some of them has absolute path which won't resolve on your machine.
If you see an assertion error, please consult Section "Obtain data and update paths".