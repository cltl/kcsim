'''
Created on Jan 18, 2015

@author: Minh Ngoc Le
'''

from logging.config import fileConfig
import sys

from nltk.corpus import wordnet as wn

from eval import simlex999, men, wordsim353
from similarity import LemmaPos2WordNetAdapter, Text2WordNetAdapter, \
    wup_similarity, lch_similarity
from eval.ordering import score_with_tie_correction


def run_taxnonomy_based():
    for sim in (wup_similarity, wn.path_similarity, lch_similarity):
        print "\n%s" %sim.__name__
        print "-"*70
        simlex999.evaluate_and_print_nv(LemmaPos2WordNetAdapter(sim))
        men.evaluate_and_print_nv(LemmaPos2WordNetAdapter(sim))
        wordsim353.evaluate_and_print(Text2WordNetAdapter(sim))
        simlex999.evaluate_and_print_high_assoc_nv(LemmaPos2WordNetAdapter(sim))
        simlex999.evaluate_groups_nv(LemmaPos2WordNetAdapter(sim), score_with_tie_correction)
        simlex999.thresholded_overlap(LemmaPos2WordNetAdapter(sim), 
                                      outpath=sim.__name__ + ".out",
                                      thresholds=(89, 108, 172, 191, 305, 645))
        sys.stdout.flush()


if __name__ == '__main__':
    fileConfig('logging.cfg')
    run_taxnonomy_based()