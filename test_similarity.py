'''
Created on Jan 26, 2015

@author: Minh Ngoc Le
'''
import unittest
from similarity import DistanceSimilarity
import numpy as np

class Test(unittest.TestCase):


    def test_distance(self):
        v1 = np.array([0.3, 0.0])
        v2 = np.array([0.0, 0.4])
        d = DistanceSimilarity.minus_distance(v1, v2)
        self.assertAlmostEqual(-0.5, float(d))
        e = np.array([[0.3, 0.0]])
        d = DistanceSimilarity.minus_distance(e[0,:], e[0,:])
        self.assertAlmostEqual(0, float(d))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_distance']
    unittest.main()