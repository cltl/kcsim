'''
Created on Apr 29, 2015

@author: Minh Ngoc Le
'''

import numpy as np
from scipy.stats import spearmanr
import matplotlib.pyplot as plt

if __name__ == '__main__':
    n = 500
    m = 1000
    gs = np.random.rand(n, m)
    tar = np.copy(gs)
    tar[n/2:] = 1-tar[n/2:]
    ab, xy = np.meshgrid(np.arange(m), np.arange(m))
    ab = ab.flatten()
    xy = xy.flatten()
    a = []
    s = []
    for k in range(n):
        switches = np.random.randint(m, size=(np.random.randint(m),2))
        p, q = switches[:,0], switches[:,1]
        tar[k,p], tar[k,q] = tar[k,q], tar[k,p]
        gs_orders = np.sign(gs[k,ab] - gs[k,xy])
        tar_orders = np.sign(tar[k,ab] - tar[k,xy])
        correct = np.sum(np.where(np.logical_or(gs_orders==0, tar_orders==0),
                                  0.5, gs_orders == tar_orders))
        a.append(correct / float(len(gs_orders)))
        s.append(spearmanr(gs[k], tar[k])[0])
    plt.scatter(a, s, s=2)
    plt.xlabel('Ordering accuracy')
    plt.ylabel("Spearman's rho")
    plt.show()