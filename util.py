'''
Created on Apr 26, 2015

@author: Minh Ngoc Le
'''
import sys


def progress(it):
    for index, value in enumerate(it):
        if (index+1) % 10000 == 0:
            sys.stderr.write("%d ...\n" %(index+1))
        yield value
        
