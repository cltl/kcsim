'''
Created on Feb 16, 2015

@author: Minh Ngoc Le
'''
import os
import sys

from eval.ordering import score_with_tie_correction
import numpy as np

# can be downloaded from: 
# https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/
# model_path = '/home/minhle/scratch/deps.words'
model_path = '/home/minh/scistor/deps.words'
assert os.path.exists(model_path)


class DepsSim(object):

    def __init__(self):
        sys.stderr.write("Loading dependency-based vector space model "
                         "from %s... " %model_path)
        self.word2index = dict()
        v = []
        with open(model_path) as f:
            for index, line in enumerate(f):
                fields = line.split(' ')
                self.word2index[fields[0]] = index
                v.append([float(s) for s in fields[1:]])
        v = np.array(v)
        self.vectors = v / np.linalg.norm(v, axis=1).reshape((-1, 1))
        sys.stderr.write("Done.\n")
        
    def __call__(self, w1, w2):
        try:
            v1 = self.vectors[self.word2index[w1]]
            v2 = self.vectors[self.word2index[w2]]
            return np.dot(v1, v2) 
        except KeyError:
            return None


if __name__ == '__main__':
    sim = DepsSim()
    from similarity import LemmaPos2LemmaAdapter
    from eval import simlex999, men, wordsim353
    simlex999.evaluate_and_print_nv(LemmaPos2LemmaAdapter(sim))
    men.evaluate_and_print_nv(LemmaPos2LemmaAdapter(sim))
    wordsim353.evaluate_and_print(sim)
    simlex999.evaluate_and_print_high_assoc_nv(LemmaPos2LemmaAdapter(sim))
    simlex999.evaluate_groups_nv(LemmaPos2LemmaAdapter(sim), score_with_tie_correction)
    simlex999.thresholded_overlap(LemmaPos2LemmaAdapter(sim), 
                                  outpath="deps.out", 
                                  thresholds=(89, 94, 108, 172, 178, 191, 305, 645))
